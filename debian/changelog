llvmlite (0.22.0-2) unstable; urgency=medium

  * deb/control:
    + pull deps of docs package to Suggests throughout.
  * deb/rules:
    + drop override for dh_auto_test (unittesting runs out of the box).
    + add override for dh_gencontrol manually adding Conflicts against the
      other dbgsym package (Closes: #868564).

 -- Daniel Stender <stender@debian.org>  Tue, 01 May 2018 13:12:50 +0200

llvmlite (0.22.0-1) unstable; urgency=medium

  * New upstream release.
  * deb/control:
    + build and run against llvm-5.0.
    + update Vcs fields (project moved to salsa).
    + bump standards to 4.1.3 (no further changes needed).
  * deb/copyright:
    + update copyright span.
    + replace full license text of CC0-1.0 with short text and pointer
      to /usr/share/common-licenses.
  * deb/rules:
    + update llvm-config path.
  * add deb/gbp.conf.

 -- Daniel Stender <stender@debian.org>  Sun, 11 Mar 2018 21:12:12 +0100

llvmlite (0.19.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Daniel Stender <stender@debian.org>  Sun, 16 Jul 2017 23:43:24 +0200

llvmlite (0.19.0-1) experimental; urgency=medium

  * New upstream release:
    + unfuzz use-system-six.patch.
  * use debhelper level 10 (changes in deb/compat and deb/control).
  * deb/copyright: use https in Format field.
  * deb/rules: add switch to regard DEB_BUILD_OPTIONS=nodoc.
  * bump standards version to 4.0.0 (no further changes needed).

 -- Daniel Stender <stender@debian.org>  Sun, 16 Jul 2017 16:30:08 +0200

llvmlite (0.18.0-1) experimental; urgency=medium

  * New upstream release.
  * deb/control:
    + bump deps to llvm-4.0.
    + build-depend on Python dev packages.
  * deb/rules: bump LLVM_CONFIG to llvm-4.0.
  * drop skip-TestDependencies.patch (now skipped by default).

 -- Daniel Stender <stender@debian.org>  Sat, 27 May 2017 12:32:36 +0200

llvmlite (0.16.0-1) unstable; urgency=medium

  * New upstream release.
  * deb/clean: fix a typo.
  * deb/control: build against llvm-3.9-dev.
  * deb/copyright: expand copyright span.
  * deb/rules: bump LLVM_CONFIG to llvm-3.9.
  * drop tests-add-libtinfo-to-expected-libs.patch.
  * add skip-TestDependencies.patch.

 -- Daniel Stender <stender@debian.org>  Fri, 26 May 2017 21:59:29 +0200

llvmlite (0.15.0+git20161228.95d8c7c-2) unstable; urgency=medium

  * Uploaded to unstable.

 -- Daniel Stender <stender@debian.org>  Thu, 29 Dec 2016 11:10:26 +0100

llvmlite (0.15.0+git20161228.95d8c7c-1) experimental; urgency=medium

  * build with latest development snapshot (Closes: #849499).

 -- Daniel Stender <stender@debian.org>  Thu, 29 Dec 2016 05:37:34 +0100

llvmlite (0.14.0-3) experimental; urgency=medium

  * remove failsafe from tests.

 -- Daniel Stender <stender@debian.org>  Sat, 24 Dec 2016 08:25:53 +0100

llvmlite (0.14.0-2) experimental; urgency=medium

  * deb/rules: add override for dh_auto_test (temporarily safed).
  * add debian/source/options (with extend-diff-ignote for ffi/*.dwo).

 -- Daniel Stender <stender@debian.org>  Tue, 20 Dec 2016 23:22:30 +0100

llvmlite (0.14.0-1) experimental; urgency=medium

  * New upstream release:
    + unfuzz use-system-six.patch.

 -- Daniel Stender <stender@debian.org>  Tue, 06 Dec 2016 20:33:59 +0100

llvmlite (0.13.0-2) experimental; urgency=medium

  * tests-add-libtinfo-to-expected-libs.patch: add more expected deps,
    needed by the supported archs.

 -- Daniel Stender <stender@debian.org>  Sun, 04 Sep 2016 18:54:23 +0200

llvmlite (0.13.0-1) experimental; urgency=medium

  * new upstream release:
    + bump llvm to 3.8 in deb/control and deb/rules (LLVM_CONFIG).
    + bump X-Python versions in deb/control.
    + add *dwo to deb/clean.
    + unfuzz and update use-system-six.patch.
    + add tests-add-libtinfo-to-expected-libs.patch.
  * deb/control:
    + remove enum34 from runtime deps (not needed).

 -- Daniel Stender <stender@debian.org>  Sat, 03 Sep 2016 18:21:06 +0200

llvmlite (0.8.0-2) experimental; urgency=medium

  * deb/control:
    + put LLVM packaging team into Maintainer field.
    + put myself into Uploaders field, updated email address.
    + don't build-depend on llvm defaults, but the specific llvm version
      which is actually supported (3.6, changed that also for binary deps).
    + add sphinx-rtd-theme to build-deps.
    + bump standards to version 3.9.8 (no changes needed).
    + add Vcs-Svn and Vcs-Browser fields (pkg-llvm).
    + drop Testsuite field (deprecated).
  * deb/copyright:
    + update maintainer email address, add missing span.
  * deb/rules:
    + add export for LLVM_CONFIG (pointing to llvm-3.6).
  * deb/watch:
    + fix filenamemangle.

 -- Daniel Stender <stender@debian.org>  Thu, 01 Sep 2016 18:25:57 +0200

llvmlite (0.8.0-1) experimental; urgency=medium

  * Initial release (Closes: #800379).

 -- Daniel Stender <debian@danielstender.com>  Mon, 14 Dec 2015 22:22:30 +0100
